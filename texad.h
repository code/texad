#include <stdlib.h>

#define INPUT_LIMIT 256
#define PROMPT "> "

enum action {
    UNKNOWN,
    LOOK,
    GO_NORTH,
    GO_SOUTH,
    GO_EAST,
    GO_WEST,
    QUIT
};

enum direction {
    NORTH,
    SOUTH,
    EAST,
    WEST
};

struct room {
    char *title;
    char *description;
    struct door **doors;
};

struct door {
    enum direction direction;
    struct room *src;
    struct room *dst;
};

struct world {
    struct player *player;
};

struct player {
    char *name;
    struct room *room;
};

struct command {
    enum action action;
    char *string;
};

struct world *genesis(void);
void apocalypse(struct world *);
enum action parse(char *);
void move(struct player *, enum direction);
void look(struct room *);
int loop(struct world *);
