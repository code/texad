.POSIX:
.PHONY: all clean
ALL = texad
all: $(ALL)
texad: texad.c texad.h
clean:
	rm -f $(ALL)
